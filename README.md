This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:
### `yarn`

Install Dependencies

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>

First Reload will get you 20 new articles from hacker news api.

After every 5 minutes time interval, it will git the api and will get the new articles ids.

If new articles are present, there will be a button on top center side of the screen.

On clicking the button, api will fetch the updated articles and will highlight in the list. 

The button disappears after the list gets updated.

The sort dropdown helps to sort by score, author or title. By default its sorted by latest article.