export const getSourceUrl = url => {
  if (!url) return "";
  const regex = /^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:/\n]+)/;
  let match = regex.exec(url);

  if (!match) return "";

  let sourceUrl = match[1] ? match[1] : "#";

  return sourceUrl;
};

export const domainLink = url => {
  if (!url) return "";
  const ssl = url.split("//")[0];
  const domain = getSourceUrl(url);
  return ssl + domain;
};
