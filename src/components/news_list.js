import React from "react";
import News from "components/news";
import "styles/news_list.css";
import {connect} from "react-redux";
import Loader from "components/loader";
import SortDropdown from "components/sort_dropdown";
import {updatedLoadedStories} from "actions/news_list";
import find from "lodash/find";

class NewsList extends React.Component {
  state = {
    newStories: [],
  };
  renderNewsItem = stories => {
    return stories.map(story => {
      const newArticle = !!find(this.state.newStories, id => id === story.id);
      // const newarticle = !!this.state.newStories.find(item => item.id === story.id );
      return (
        <div key={story.id}>
          <News {...story} highlight={newArticle} />
        </div>
      );
    });
  };

  handleStoryUpdate = () => {
    this.setState({
      newStories: this.props.newStories,
    });
    this.props.updatedLoadedStories();
  };
  render() {
    const {stories, sortedStories, newStories} = this.props;
    const storyArray = sortedStories.length !== 0 ? sortedStories : stories;
    return (
      <div className="NewsList">
        <div className="header">
          <h1>NewsList</h1>
          {newStories.length !== 0 ? (
            <h2 className="new-stories" onClick={this.handleStoryUpdate}>
              {newStories.length} New Stories
            </h2>
          ) : null}
          <SortDropdown />
        </div>
        <Loader />
        {this.renderNewsItem(storyArray)}
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  stories: state.newsList.stories,
  sortedStories: state.newsList.sortedStories,
  newStories: state.newsList.newStories,
});

export default connect(
  mapStateToProps,
  {updatedLoadedStories},
)(NewsList);
