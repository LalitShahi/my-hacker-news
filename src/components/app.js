import React from "react";
import "styles/app.css";
import NewsList from "components/news_list";
import {connect} from "react-redux";
import {fetchStoryIds, pollNewStoryIds} from "actions/news_list";
import {withPolling} from "components/with_polling";
class App extends React.Component {
  componentDidMount() {
    this.props.fetchStoryIds();
  }
  render() {
    return (
      <div className="App">
        <NewsList />
      </div>
    );
  }
}

export default withPolling(pollNewStoryIds)(
  connect(
    null,
    {fetchStoryIds},
  )(App),
);
