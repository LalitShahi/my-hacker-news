import React from "react";
import "styles/news.css";
import {getSourceUrl, domainLink} from "utils/source_url";
import TimeAgo from "react-timeago";

const News = props => {
  const sourceDomain = getSourceUrl(props.url);
  const sourceDomainLink = domainLink(props.url);
  const articleLink = props.url;
  return (
    <div className={props.highlight ? "newStory" : "news"}>
      <div className="news-header">
        <a href={articleLink}>
          <h2>{props.title}</h2>
        </a>
        <span>
          (
          <a href={sourceDomainLink}>
            {sourceDomain ? sourceDomain : "Not Available"}
          </a>
          )
        </span>
      </div>
      <div className="news-details">
        <p>
          {props.score} Points By {props.by}{" "}
          {<TimeAgo date={props.time * 1000} />}
        </p>
        <p> | {props.kids ? props.kids.length : 0} Comments</p>
      </div>
    </div>
  );
};

export default News;
