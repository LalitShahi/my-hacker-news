import React from "react";
import {GridSpinner} from "react-spinners-kit";
import {connect} from "react-redux";
import "styles/news_list.css";

class Loader extends React.Component {
  render() {
    return (
      <div className="loader">
        <GridSpinner size={30} color="#ddd" loading={this.props.isFetching} />
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  isFetching: state.newsList.isFetching,
});

export default connect(mapStateToProps)(Loader);
