import React from "react";
import "styles/sort_dropdown.css";
import {setSortBy} from "actions/news_list";
import {connect} from "react-redux";

class SortDropdown extends React.Component {
  saveSelectValue = e => {
    const value = e.target.value;

    this.props.setSortBy(value);
  };
  render() {
    return (
      <select className="dropdown" onChange={this.saveSelectValue}>
        <option value="score" defaultValue>
          Score
        </option>
        <option value="author">Author</option>
        <option value="title">Title</option>
      </select>
    );
  }
}

export default connect(
  null,
  {setSortBy},
)(SortDropdown);
