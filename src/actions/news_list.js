import {
  FETCH_NEWS_ID_REQUEST,
  FETCH_NEWS_ID_SUCCESS,
  FETCH_NEWS_ID_FAILURE,
  FETCH_NEWS_BY_ID_REQUEST,
  FETCH_NEWS_BY_ID_SUCCESS,
  FETCH_NEWS_BY_ID_FAILURE,
  SET_SORT_BY,
  SORT_NEWS_FROM_DROPDOWN,
  POLL_NEWS_ID_SUCCESS,
  POLL_NEWS_ID_FAILURE,
  RESET_NEW_STORIES,
} from "constants/action_types/news_list";

import hackerNewsApi from "services/hacker_news_api";
import sortBy from "lodash/sortBy";
import difference from "lodash/difference";

export const fetchNewStoriesById = payload => (dispatch, getState) => {
  const {ids, page} = payload;
  dispatch({
    type: FETCH_NEWS_BY_ID_REQUEST,
  });
  return hackerNewsApi
    .getStoriesByPage(ids, page)
    .then(stories => {
      const updatedStories = [...stories];
      dispatch({
        type: FETCH_NEWS_BY_ID_SUCCESS,
        stories: updatedStories,
      });
      dispatch(sortStories(getState().newsList.sortBy));
    })
    .catch(error =>
      dispatch({
        type: FETCH_NEWS_BY_ID_FAILURE,
        error,
      }),
    );
};

export const fetchStoryIds = () => dispatch => {
  dispatch({
    type: FETCH_NEWS_ID_REQUEST,
  });
  return hackerNewsApi
    .getNewStoryIds()
    .then(ids => {
      dispatch({
        type: FETCH_NEWS_ID_SUCCESS,
        ids,
      });
      const payload = {ids, page: 0};
      dispatch(fetchNewStoriesById(payload));
      return ids;
    })
    .catch(error =>
      dispatch({
        type: FETCH_NEWS_ID_FAILURE,
        error,
      }),
    );
};

export const pollNewStoryIds = () => (dispatch, getState) => {
  return hackerNewsApi
    .getNewStoryIds()
    .then(ids => {
      const loadedStoryIds = [...getState().newsList.storyIds];
      const newStories = difference(ids, loadedStoryIds);
      dispatch({
        type: POLL_NEWS_ID_SUCCESS,
        newStories: newStories,
      });
    })
    .catch(error =>
      dispatch({
        type: POLL_NEWS_ID_FAILURE,
        error,
      }),
    );
};

export const setSortBy = value => dispatch => {
  if (value === "author") value = "by";
  dispatch({
    type: SET_SORT_BY,
    value,
  });
  dispatch(sortStories(value));
};

export const sortStories = sortByKey => (dispatch, getState) => {
  const stories = getState().newsList.stories;
  let sortedStories = [...getState().newsList.sortedStories];
  sortedStories = sortBy(stories, story => story[sortByKey]);
  dispatch({
    type: SORT_NEWS_FROM_DROPDOWN,
    sortedStories,
  });
};

export const updatedLoadedStories = () => dispatch => {
  dispatch(fetchStoryIds());
  dispatch({
    type: RESET_NEW_STORIES,
  });
};
