import {combineReducers} from "redux";
import newsList from "./news_list";

const rootReducer = combineReducers({
  newsList,
});

export default rootReducer;
