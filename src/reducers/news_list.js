import {
  FETCH_NEWS_ID_REQUEST,
  FETCH_NEWS_ID_SUCCESS,
  FETCH_NEWS_ID_FAILURE,
  FETCH_NEWS_BY_ID_REQUEST,
  FETCH_NEWS_BY_ID_SUCCESS,
  FETCH_NEWS_BY_ID_FAILURE,
  SORT_NEWS_FROM_DROPDOWN,
  SET_SORT_BY,
  POLL_NEWS_ID_SUCCESS,
  POLL_NEWS_ID_FAILURE,
  RESET_NEW_STORIES,
} from "constants/action_types/news_list";

const intitialState = {
  storyIds: [],
  stories: [],
  sortedStories: [],
  sortBy: "",
  newStories: [],
  page: 0,
  isFetching: false,
  error: "",
};

export default (state = intitialState, action) => {
  switch (action.type) {
    case FETCH_NEWS_ID_REQUEST:
    case FETCH_NEWS_BY_ID_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case FETCH_NEWS_ID_SUCCESS:
      return {
        ...state,
        storyIds: action.ids,
      };
    case FETCH_NEWS_BY_ID_SUCCESS:
      return {
        ...state,
        stories: action.stories,
        page: state.page + 1,
        isFetching: false,
      };
    case FETCH_NEWS_ID_FAILURE:
    case FETCH_NEWS_BY_ID_FAILURE:
    case POLL_NEWS_ID_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    case SET_SORT_BY:
      return {
        ...state,
        sortBy: action.value,
      };
    case SORT_NEWS_FROM_DROPDOWN:
      return {
        ...state,
        sortedStories: action.sortedStories,
      };
    case POLL_NEWS_ID_SUCCESS:
      return {
        ...state,
        newStories: action.newStories,
      };
    case RESET_NEW_STORIES:
      return {
        ...state,
        newStories: [],
      };
    default:
      return state;
  }
};
